package testRunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(features="features",
        glue={"step_definitions"},
        monochrome = false,
        tags = "@Smoke or @Test or ~@WIP",
        plugin = "html:resources/report/report.html")

public class TestRun {
}

