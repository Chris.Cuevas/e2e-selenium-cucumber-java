package step_definitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.Dashboard;
import page.LoginPage;

import static org.junit.Assert.assertTrue;

public class Login  {

    WebDriver driver;
    public LoginPage loginPage;
    private Dashboard dashboard;

    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver","drivers/for_windows/chromedriver_v87.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        loginPage = new LoginPage(driver);
    }

    @Given("the user is at the login page")
    public void the_user_is_at_the_login_page() {
        driver.get("https://admin-demo.nopcommerce.com/admin");
    }

    @When("the user login using the following details")
    public void login(DataTable table){

        var user = table.row(1);
        loginPage.enterEmail(user.get(0));
        loginPage.enterPassword(user.get(1));
        dashboard = loginPage.submit();
    }

    @Then("the user should be at the dashboard")
    public void the_user_should_be_at_the_dashboard(){
        assertTrue(dashboard.isDashboardDisplayed());
    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
