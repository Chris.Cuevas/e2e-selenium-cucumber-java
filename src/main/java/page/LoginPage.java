package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    WebDriver driver;

    public LoginPage(WebDriver driver){
        this.driver = driver;
    }

    private By emailField = By.id("Email");
    private By passwordField = By.id("Password");
    private By submitButton = By.cssSelector(".login-button");

    public void enterEmail(String email){
        driver.findElement(emailField).clear();
        driver.findElement(emailField).sendKeys(email);
    }

    public void enterPassword(String password){
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(password);
    }

    public Dashboard submit() {
        driver.findElement(submitButton).click();
        return new Dashboard(driver);
    }
}
