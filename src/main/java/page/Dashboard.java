package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Dashboard {

    WebDriver driver;

    public Dashboard(WebDriver driver){
        this.driver = driver;
    }

    private By userNameLabel = By.id("search-box");
    private By logoutBtn = By.linkText("Logout");

    public boolean isDashboardDisplayed(){
        return driver.findElement(userNameLabel).isDisplayed();
    }
}
