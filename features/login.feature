@Test
Feature: Login

  Scenario: User login using valid credentials
    Given the user is at the login page
     When the user login using the following details
         | email               | password |
         | admin@yourstore.com | admin    |
     Then the user should be at the dashboard

  Scenario Outline: User login using invalid credentials
      Given the user is at the login page
      When the user login using the following details
        | email   | password      |
        | <email> | <password>    |
      Then the user should be at the dashboard

      Examples:
      | email                      | password |
      | natalee_sod@yourstore.com  | admin    |
      | bentylador@yourstore.com   | admin    |
